from django.shortcuts import render
from pcode.conf_lib import TableConfig

# Create your views here.


def view_table(request):
    config = TableConfig("sportbot.xml")
    sites = config.get_sites()
    teams = config.get_teams()

    return render(request, 'qtable/table_page.html',
                  {'sites': sites, 'teams': teams})

