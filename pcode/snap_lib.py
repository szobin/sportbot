#!/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import json
import xml_lib


def get_site_content(url, options_tag=None):
    if (url.count("http:") == 0) and (url.count("https:") == 0):
        url = "http://" + url

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0',
               'Accept-Language': "en-US,en;q=0.8"
               }

    proxies = None
    if options_tag is not None:
        proxy_str = xml_lib.xml_str_param(options_tag, "proxy")
        proxies = {'http:': proxy_str, 'https': proxy_str}

    try:
        if proxies is None:
            r = requests.get(url, headers=headers)
        else:
            r = requests.get(url, headers=headers, proxies=proxies)
        return r.status_code, r.text, ""
    except Exception as ex:
        return 500, "", str(ex)


def store_file(fn, content):
    try:
        f = open(fn, 'w')
        try:
            f.write(content.encode('utf-8'))
            return True, ""
        finally:
            f.close()
    except Exception as ex:
        return False, str(ex)


def extract_text_by_options(site_content, options_tag):
    start_id = 1
    while True:
        start_text = xml_lib.xml_str_param(options_tag, "start" + str(start_id))
        if len(start_text) == 0:
            break
        p = site_content.find(start_text)
        if p < 0:
            break
        site_content = site_content[p:]
        start_id += 1

    stop_id = 1
    while True:
        stop_text = xml_lib.xml_str_param(options_tag, "stop" + str(stop_id))
        if len(stop_text) == 0:
            break
        p = site_content.rfind(stop_text)
        if p < 0:
            break
        site_content = site_content[:p+len(stop_text)]
        stop_id += 1
    return site_content


def has_json_option(options_tag):
    return xml_lib.xml_str_param(options_tag, "format") == 'json'


def has_text_option(options_tag):
    return xml_lib.xml_str_param(options_tag, "format") == 'text'


def has_dyn_option(options_tag):
    return xml_lib.xml_str_param(options_tag, "mode") == 'dyn'


def load_json(json_content):
    try:
        json_data = json.loads(json_content)
    except Exception as ex:
        json_data = dict(error=str(ex))
    return json_data


def store_json(json_data, fn):
    try:
        json_str = json.dumps(json_data)
        store_file(fn, json_str)
        return True
    except Exception as ex:
        return False


def get_json_list(json_data, options_tag):
    list_name = xml_lib.xml_str_param(options_tag, "list")
    if list_name in json_data:
        json_list = json_data[list_name]
    else:
        json_list = []
    return json_list


def get_json_name_value(json_item, options_tag, name, default=""):
    item_name = xml_lib.xml_str_param(options_tag, name)
    if item_name in json_item:
        item_value = json_item[item_name]
        return item_value
    else:
        return default


def get_text_value(text, p, options_tag, name_start, name_stop):
    value_start = xml_lib.xml_str_param(options_tag, name_start)
    if value_start == '':
        return "", p

    p = text.find(value_start, p)
    if p < 0:
        return "", p
    p += len(value_start)

    value_stop = xml_lib.xml_str_param(options_tag, name_stop)
    if value_stop == '':
        return "", p

    p2 = text.find(value_stop, p)
    if p2 < 0:
        return "", p

    t = text[p:p2]
    return t.strip(), p2+len(value_stop)

