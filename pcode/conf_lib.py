#!/usr/bin/python
# -*- coding: utf-8 -*-
from lxml import etree
from pcode import conf, xml_lib, snap_lib


class TableConfig:
    config = None
    teams = None

    def __init__(self, config_fn='config.xml', team_fn="team.json", debug=True):
        self.teams = dict()
        self.team_fn = team_fn
        try:
            fn = conf.data_path(config_fn)
            f = open(fn, 'r')
            try:
                xmlstr = f.read()
            finally:
                f.close()
            self.config = etree.fromstring(xmlstr)
        except Exception as ex:
            self.config = etree.Element("config", interval="300", err=str(ex))
            return

        try:
            fn = conf.data_path(team_fn)
            f = open(fn, 'r')
            try:
                team_str = f.read()
                self.teams = snap_lib.load_json(team_str)
            finally:
                f.close()
        except Exception as ex:
            self.teams = dict()

        try:
            if debug and len(self.config) > 0:
                self.load_teams()
        except Exception as ex:
            self.config = etree.Element("config", interval="300", err=str(ex))
            return



    def get_sites(self):
        ss = list()

        err = xml_lib.xml_str_param(self.config, "err")
        if len(err) > 0:
            ss.append(["error", err])
        for site_tag in self.config:
            ss.append([xml_lib.xml_str_param(site_tag, "name", 'N/A'),
                       xml_lib.xml_str_param(site_tag, "url"),
                       xml_lib.xml_int_param(site_tag, "site_code", 0)])
        return ss

    def get_teams(self):
        return sorted(self.teams.items())

    def append_team_value(self, team_name, site_no, team_value):
        if team_name in self.teams:
            self.teams[team_name][site_no] = team_value
        else:
            teams_row = [""] * (len(self.config))
            teams_row[site_no] = team_value
            self.teams.update({team_name: teams_row})
        return

    def load_teams(self):
        if len(self.teams) > 0:
            return

        site_no = -1
        self.teams = dict()
        for site_tag in self.config:
            site_no += 1
            self.load_site_team_info(site_no, site_tag)
        fn = conf.data_path(self.team_fn)
        snap_lib.store_json(self.teams, fn)
        return

    def load_site_team_info(self, site_no, site_tag):
        name = xml_lib.xml_str_param(site_tag, "name")
        url = xml_lib.xml_str_param(site_tag, "url")
        url = xml_lib.update_url(url)

        if len(site_tag) == 0:
            options_tag = None
        else:
            options_tag = site_tag[0]

        (site_code, site_text, site_err) = snap_lib.get_site_content(url, options_tag)
        if site_code != 200:
            site_tag.set('site_code', str(site_code))
            site_tag.set('err', site_err)
            return

        part_text = snap_lib.extract_text_by_options(site_text, options_tag)
        if snap_lib.has_json_option(options_tag):
            json_data = snap_lib.load_json(part_text)
            data_list = snap_lib.get_json_list(json_data, options_tag)
            if len(data_list) == 0:
                fn = conf.data_path(name + '.part')
                snap_lib.store_file(fn, part_text)
                return

            for item in data_list:
                team_name = snap_lib.get_json_name_value(item, options_tag, "team")
                if len(team_name) == 0:
                    continue

                v1 = snap_lib.get_json_name_value(item, options_tag, "value1")
                v2 = snap_lib.get_json_name_value(item, options_tag, "value2")
                self.append_team_value(team_name, site_no, v1 + ' / ' + v2)
            return

        if snap_lib.has_text_option(options_tag):
            p = 0
            while True:
                (team_name, p) = snap_lib.get_text_value(part_text, p, options_tag, "item_team_start", "item_team_stop")
                if len(team_name) == 0:
                    break
                (team_value, p) = snap_lib.get_text_value(part_text, p, options_tag, "item_value_start", "item_value_stop")
                if len(team_value) == 0:
                    break
                self.append_team_value(team_name, site_no, team_value)
            return

        fn = conf.data_path(name + '.html')
        snap_lib.store_file(fn, site_text)
        return
