#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.conf import settings
import os


def data_path(fn):
    path = os.path.join(settings.BASE_DIR, 'data')
    if not os.path.exists(path):
        os.makedirs(path)
    return os.path.join(path, fn)
